FROM php:8.1-fpm

    # copy php.ini-development to php.ini (cf official image doc.)
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini" && \
    # install Composer using curl
    curl -sS https://getcomposer.org/installer | php -- --filename=composer --install-dir=/usr/local/bin && \
    # get symfony setup (cf symfony docs.)
    curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.deb.sh' | bash && \
    # install required packages using apt.
    apt update && apt install -y zip git libicu-dev symfony-cli locales && \
    # install php extensions via docker-php-ext-install command (cf official image doc.)
    docker-php-ext-install opcache intl pdo_mysql && \
    # install fr_FR locale
    echo "fr_FR.UTF-8 UTF-8" >> /etc/locale.gen && \
    locale-gen

WORKDIR /app

# just copy composer.json to not invalidate cache
COPY . .
    
RUN composer install --no-dev --optimize-autoloader && \
    APP_ENV=prod APP_DEBUG=0 php bin/console cache:clear && \
    usermod -aG sudo www-data
